import { library } from '@fortawesome/fontawesome-svg-core'
import { faInfoCircle, faChevronLeft, faChevronDown, faChevronRight, faCheckCircle } from '@fortawesome/free-solid-svg-icons'

library.add(faInfoCircle, faChevronLeft, faChevronDown, faChevronRight, faCheckCircle);
