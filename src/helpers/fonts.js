import { css } from 'styled-components';
import SFProRegular from '../assets/fonts/SFProDisplay-Regular.woff'
import SFProRegular2 from '../assets/fonts/SFProDisplay-Regular.woff2'
import SFProSemiBold from '../assets/fonts/SFProDisplay-Semibold.woff'
import SFProSemiBold2 from '../assets/fonts/SFProDisplay-Semibold.woff2'

export default css`
  @font-face {
    font-family: 'SF Pro';
    font-weight: 400;
    src: url(${SFProRegular}), url(${SFProRegular2});
  }
  @font-face {
    font-family: 'SF Pro';
    font-weight: 600;
    src: url(${SFProSemiBold}), url(${SFProSemiBold2});
  }
`;
