export default {
  white: '#FFFFFF',
  red: '#DE4B4B',
  shadow: 'rgba(0, 0, 0, 0.7)',
  lightGray: '#C9C9C9',
  error: '#EB5757',
  darkGray: '#3C3C3C',
}
