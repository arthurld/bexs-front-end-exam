import React from 'react';
import styled from 'styled-components';

import Aside from '../organisms/CheckoutAside'
import Content from '../organisms/CheckoutContent'

const Grid = styled.div`
  display: grid;
  grid-template-columns: 352px 1fr;
  min-height: 100%;
  
  @media(max-width: 980px) {
    grid-template-columns: 1fr;
    grid-template-rows: 240px 1fr;
  }
`;

const GridAside = styled.div`
  width: 100%;
  background-color: ${props => props.theme.red};
`;

const GridContent = styled.div`
  width: 100%;
  background-color: ${props => props.theme.white};
`;

const Checkout = () => (
  <Grid>
    <GridAside>
      <Aside />
    </GridAside>
    <GridContent>
      <Content />
    </GridContent>
  </Grid>
);

export default Checkout;
