import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import IconBase from "../atoms/Icon";

const StyledTitle = styled.h1`
  font-size: 20px;
  display: flex;
  align-items: center;
`;

const Icon = styled(IconBase)`
  margin-right: 16px;
  font-size: 50px;
  width: 50px;
`;

const TextContainer = styled.div`
  flex: 1;
`;

// For icons to work you need add them to the library first. See in helpers folder
const Title = ({ iconName, children, ...props }) => {
  return (
    <StyledTitle {...props}>
      {iconName && <Icon name={iconName} />}
      <TextContainer>{children}</TextContainer>
    </StyledTitle>
  );
};

Title.propTypes = {
  iconName: PropTypes.string,
  children: PropTypes.string.isRequired,
};

Title.defaultProps = {
  iconName: '',
};

export default Title;
