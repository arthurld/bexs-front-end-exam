import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LabelBase from "./Label";
import ErrorMessage from "../atoms/ErrorMessage";
import InputBase from "../atoms/Input";

const Label = styled(LabelBase)``;
const Input = styled(InputBase)``;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  width: ${props => (props.half ? '48%' : '100%')};
  min-height: 65px;
  padding-top: 15px;
  margin: 10px 0;
  
  ${Label} {
    position: absolute;
    top: 0;
    left: 0;
    transform: translateY(21px);
    transition: 0.4s all ease;
    ${props => props.touched && `
      transform: translateY(0px);
      font-size: 13px;
    `}
  };
  
  ${Input}:focus + ${Label} {
    transform: translateY(0px);
    font-size: 13px;
  }
  
  ${ErrorMessage} {
    margin-top: 4px;
  };
`;

const Field = ({ label, info, error, half, name, touched, ...props }) => {
  return (
    <Container half={half} touched={touched}>
      <Input isInvalid={!!error} name={name} id={name} {...props} />
      <Label htmlFor={name} info={info}>{label}</Label>
      {error && <ErrorMessage>{error}</ErrorMessage>}
    </Container>
  );
};

Field.propTypes = {
  label: PropTypes.string.isRequired,
  info: PropTypes.string,
  error: PropTypes.string,
  half: PropTypes.bool,
  name: PropTypes.string.isRequired,
  touched: PropTypes.bool.isRequired,
};

Field.defaultProps = {
  info: '',
  error: '',
  half: false,
  name: false,
  touched: false,
};

export default Field;
