import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import IconBase from '../atoms/Icon';

// Normally here wold be a Link from react-router
const StyledLink = styled.a`
  display: flex;
  align-items: center;
  color: ${props => props.theme.white};
`;

const Icon = styled(IconBase)`
  ${props => props.position === 'left'
  ? `
    margin-right: 16px;
  ` : `
    margin-left: 16px;
  `};
`;

const Link = ({ iconPosition, children, ...props }) => (
  <StyledLink {...props}>
    {iconPosition === 'left' && <Icon position={iconPosition} name="chevron-left" />}
    {children}
    {iconPosition === 'right' && <Icon position={iconPosition} name="chevron-right" />}
  </StyledLink>
);

Link.propTypes = {
  iconPosition: PropTypes.string,
  children: PropTypes.string.isRequired,
};

Link.defaultProps = {
  iconPosition: 'left'
};

export default Link;
