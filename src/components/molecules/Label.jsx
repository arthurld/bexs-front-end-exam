import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Icon from '../atoms/Icon';

const StyledLabel = styled.label`
  font-size: 17px;
  color: ${props => props.theme.lightGray};
  display: flex;
  align-items: center;
`;

const Info = styled(Icon).attrs({
  name: 'info-circle',
})`
  font-size: 13px;
  fill: ${props => props.theme.lightGray};
  margin-left: 8px;
`;

const Label = ({ info, children, ...props }) => (
  <StyledLabel {...props}>
    {children}
    {info && <Info title={info} />}
  </StyledLabel>
);

Label.propTypes = {
  info: PropTypes.string,
  children: PropTypes.string.isRequired,
};

Label.defaultProps = {
  info: '',
};

export default Label;
