import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import VisaFrontBg from '../../assets/backgrounds/card-visa@2x.png';
import VisaBackBg from '../../assets/backgrounds/back-card-visa@2x.png';
import EmptyFrontBg from '../../assets/backgrounds/empty-card@2x.png';
import EmptyBackBg from '../../assets/backgrounds/back-empty-card@2x.png';
import VisaLogo from '../../assets/logos/logo-visa.png';
import VisaLogo2x from '../../assets/logos/logo-visa@2x.png';

const Cards = {
  empty: {
    back: EmptyBackBg,
    front: EmptyFrontBg,
  },
  visa: {
    flag: [VisaLogo, VisaLogo2x],
    back: VisaBackBg,
    front: VisaFrontBg,
  },
};

const Face = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 10px;
  backface-visibility: hidden;
  position: absolute;
  padding: 36px 24px;
  transition: 0.4s background ease;
  background-size: cover;
  overflow: hidden;
`;

const Front = styled(Face)`
  background-image: url(${props => Cards[props.cardType].front});
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

const Back = styled(Face)`
  background-image: url(${props => Cards[props.cardType].back});
  z-index: -1;
  transform: rotateY(180deg);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Flag = styled.img`
  height: 22px;
  max-width: 72px;
  object-fit: cover;
  margin-bottom: 48px;
`;

const CardNumber = styled.span`
  font-size: 24px;
  font-family: 'SF Pro';
  color: ${props => props.theme.white};
  letter-spacing: 3px;
  text-shadow: 0 2px 2px ${props => props.theme.shadow};
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 36px;
`;

const Info = styled.span`
  font-family: 'SF Pro';
  letter-spacing: 1px;
  font-size: 16px;
  color: ${props => props.theme.white};
  text-shadow: 0 2px 2px ${props => props.theme.shadow};
  text-transform: uppercase;
`;

const Name = styled(Info)`
  flex: 1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-right: 8px;
`;

const Cvv = styled.span`
  font-size: 18px;
  font-family: 'SF Pro';
  color: ${props => props.theme.darkGray};
  letter-spacing: 5px;
  text-shadow: 0 2px 2px ${props => props.theme.shadow};
  transform: translate(15px, 6px);
`;

const NumberContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledCard = styled.div`
  width: 364px;
  height: 224px;
  transition: 0.6s transform ease;
  transform: rotateY(${props => props.flipped ? '180deg' : '0deg'});
  transform-style: preserve-3d;
  box-shadow: 0 23px 20px -15px rgba(0,0,0, 0.5);
  border-radius: 10px;
  
  @media(max-width: 980px) {
    width: 280px;
    height: 172px;
    
    ${Face} {
      padding: 28px 16px;
    }
    
    ${CardNumber} {
      font-size: 19px;
      letter-spacing: 3px;
    }
    
    ${Row} {
      margin-top: 24px;
    }
    
    ${Flag} {
      margin-bottom: 32px;
    }
  }
`;

const Card = ({ flipped, number, name, expireDate, cvv, ...props }) => {
  const cardNumber = number.padEnd(16, '*').replace(/(.{4})/g, '$1 ').trim();
  const cardNumberArray = cardNumber.split(' ');
  const maskedCvv = cvv.replace(/\d/g, '*');
  const type = number ? 'visa' : 'empty';
  return (
    <StyledCard flipped={flipped} {...props}>
      <Front cardType={type}>
        {Cards[type].flag && <Flag src={Cards[type].flag[0]} srcSet={`${Cards[type].flag[0]} 1x, ${Cards[type].flag[1]} 2x`} />}
        <NumberContainer>
          {cardNumberArray.map(number => <CardNumber>{number}</CardNumber>)}
        </NumberContainer>
        <Row>
          <Name>{name}</Name>
          <Info>{expireDate}</Info>
        </Row>
      </Front>
      <Back cardType={type}>
        <Cvv>{maskedCvv}</Cvv>
      </Back>
    </StyledCard>
  );
};

Card.propTypes = {
  flipped: PropTypes.bool,
  number: PropTypes.string,
  name: PropTypes.string,
  expireDate: PropTypes.string,
  cvv: PropTypes.string,
};

Card.defaultProps = {
  number: '',
  name: 'NOME DO TITULAR',
  expireDate: '00/00',
  flipped: false,
  cvv: '',
};

const mapper = stores => ({
  number: stores.card.number.get(),
  name: stores.card.name.get(),
  expireDate: stores.card.expireDate.get(),
  cvv: stores.card.cvv.get(),
  flipped: stores.card.flipped.get(),
});

export default inject(mapper)(observer(Card));
