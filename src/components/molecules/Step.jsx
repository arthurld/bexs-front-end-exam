import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import StepBullet from '../atoms/StepBullet';
import StepLabelBase from '../atoms/StepLabel';

const Container = styled.div`
  display: flex;
  align-items: center;
  color: ${props => props.theme.red};
`;

const StepLabel = styled(StepLabelBase)`
  margin-left: 8px;
`;

const Step = ({ completed, step, label }) => {
  return (
    <Container>
      <StepBullet completed={completed} step={step}/>
      <StepLabel>{label}</StepLabel>
    </Container>
  );
};

Step.propTypes = {
  step: PropTypes.number.isRequired,
  completed: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
};

export default Step;
