import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withFormik } from "formik";
import { inject } from 'mobx-react';
import { toast } from 'react-toastify';
import api from '../../services/api';

import Field from "../molecules/Field";
import ButtonBase from "../atoms/Button";

const Button = styled(ButtonBase)`
  margin-top: 40px;
  margin-left: auto;
`;

const StyledForm = styled.form`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  
  @media(max-width: 980px) {
    ${Button} {
      margin-top: 20px;
      margin-left: 0;
      width: 100%;
    }
  }
`;

class Form extends React.Component {
  renderOptions = (installments, total) => {
    const options = [];
    for (let i = 1; i <= installments; i++) {
      const price = (Math.round((total / i) * 100) / 100)
        .toFixed(2)
        .replace('.', ',')
        .replace(/\d(?=(\d{3})+,)/g, '$&.');
      options.push(<option key={i} value={i}>{`${i}x de R$ ${price} sem juros`}</option>);
    }
    return options;
  };

  componentDidUpdate() {
    const {
      setNumber,
      setCvv,
      setExpireDate,
      setName,
      values
    } = this.props;
    setNumber(values.card_number);
    setCvv(values.cvv);
    setExpireDate(values.expire_date);
    setName(values.name);
  }

  render() {
    const {
      values,
      touched,
      errors,
      handleBlur,
      handleChange,
      handleSubmit,
      setFlipped,
      isSubmitting,
      ...props
    } = this.props;
    return (
      <StyledForm {...props} onSubmit={handleSubmit}>
        <Field
          value={values.card_number}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={touched.card_number}
          error={touched.card_number && errors.card_number}
          mask={[/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/]}
          label="Número do cartão"
          type="text"
          name="card_number"
        />
        <Field
          value={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={touched.name}
          error={touched.name && errors.name}
          label="Nome (igual ao cartão)"
          type="text"
          name="name"
        />
        <Field
          value={values.expire_date}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={touched.expire_date}
          error={touched.expire_date && errors.expire_date}
          mask={[/[0-1]/, /\d/, '/', /\d/, /\d/]}
          half
          label="Validade"
          type="text"
          name="expire_date"
        />
        <Field
          value={values.cvv}
          onChange={handleChange}
          onBlur={(e) => {
            handleBlur(e);
            setFlipped(false);
          }}
          onFocus={() => setFlipped(true)}
          touched={touched.cvv}
          error={touched.cvv && errors.cvv}
          mask={[/\d/, /\d/, /\d/]}
          half
          label="CVV"
          type="text"
          name="cvv"
          info="São os 3 digítos que sem encontram no versão do cartão"
        />
        <Field
          value={values.installments}
          onChange={handleChange}
          onBlur={handleBlur}
          touched={touched.installments}
          error={touched.installments && errors.installments}
          label="Número de parcelas"
          name="installments" type="select"
        >
          <option value="" />
          {this.renderOptions(12, 12000)}
        </Field>
        <Button isLoading={isSubmitting} type="submit">Continuar</Button>
      </StyledForm>
    );
  }
}

Form.propTypes = {
  values: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  handleBlur: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  setNumber: PropTypes.func.isRequired,
  setName: PropTypes.func.isRequired,
  setExpireDate: PropTypes.func.isRequired,
  setCvv: PropTypes.func.isRequired,
  setFlipped: PropTypes.func.isRequired,
};

const mapper = stores => ({
  setNumber: number => stores.card.setNumber(number),
  setName: name => stores.card.setName(name),
  setExpireDate: date => stores.card.setExpireDate(date),
  setCvv: number => stores.card.setCvv(number),
  setFlipped: value => stores.card.setFlipped(value),
});

export default withFormik({
  mapPropsToValues: () => ({
    card_number: '',
    name: '',
    expire_date: '',
    cvv: '',
    installments: '',
  }),
  validateOnBlur: true,
  validateOnChange: true,
  validate: (values) => {
    const errors = {};
    if (!values.card_number) {
      errors.card_number = 'Número de cartão é inválido.';
    } else if (!values.card_number.match(/^4/)) {
      errors.card_number = 'Número de cartão é inválido.';
    } else if (values.card_number.replace(/\D*/g, '').length < 16) {
      errors.card_number = 'Número de cartão é inválido.';
    }

    if (!values.name.match(/\w{2,}\s\w+/)) {
      errors.name = 'Insira seu nome completo.'
    }

    if (!values.expire_date) {
      errors.expire_date = 'Data inválida.';
    } else if(values.expire_date.length === 5) {
      const date = values.expire_date.split('/');
      const currentYear = new Date().getFullYear().toString().replace(/^\d{2}/, '');
      const currentMonth = (new Date().getMonth() + 1).toString();

      if (date[1] === currentYear && parseInt(date[0]) < parseInt(currentMonth)) {
        errors.expire_date = 'Data inválida.';
      }

      if (date[0] > 12 || date[1] < currentYear) {
        errors.expire_date = 'Data inválida.';
      }
    }

    if (values.cvv.replace(/\D*/g, '').length < 3) {
      errors.cvv = 'CVV inválido.';
    }

    if (!values.installments) {
      errors.installments = 'Número de parcelas inválido.'
    }

    return errors;
  },
  handleSubmit: async (values, { setSubmitting  }) => {
    setSubmitting(true);
    try {
      const request = await api.post('/pagar', values);
      console.log(request);
    } catch (e) {
      toast.error('Algo deu errado, tente novamente mais tarde');
    } finally {
      setSubmitting(false);
    }
  },
})(inject(mapper)(Form));
