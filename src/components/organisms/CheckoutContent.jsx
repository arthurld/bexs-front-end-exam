import React from 'react';
import styled from 'styled-components';
import Icon from "../atoms/Icon";
import Step from "../molecules/Step";
import FormBase from "./Form";

const Steps = styled.div`
  display: flex;
  align-items: center;
  align-self: flex-end;
`;

const StepSeparator = styled(Icon).attrs({
  name: 'chevron-right'
})`
  margin: 0 16px;
  color: ${props => props.theme.red};
`;

const Form = styled(FormBase)`
  margin-top: 54px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 56px;
  max-width: 440px;
  margin-left: 168px;
  
  @media(max-width: 980px) {
    margin-left: 0;
    padding: 132px 40px 0;
    align-items: center;
    max-width: 100%;

    ${Steps} {
      display: none;
    }
    
    ${Form} {
      margin-top: 0;
    }
  }
`;

const CheckoutContent = () => {
  return (
    <Container>
      <Steps>
        <Step completed label="Carrinho" step={1} />
        <StepSeparator />
        <Step label="Pagamento" step={2} completed={false} />
        <StepSeparator />
        <Step label="Confirmação" step={3} completed={false} />
      </Steps>
      <Form />
    </Container>
  );
};

export default CheckoutContent;
