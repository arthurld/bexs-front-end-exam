import React from 'react';
import styled from 'styled-components';

import LinkBase from '../molecules/Link';
import TitleBase from '../molecules/Title';
import CardBase from '../molecules/Card'
import SimpleProgressionBase from '../atoms/SimpleProgression';

const Link = styled(LinkBase)``;
const SimpleProgression = styled(SimpleProgressionBase)``;

const Title = styled(TitleBase)`
  color: ${props => props.theme.white};
  margin-top: 54px;
`;

const Card = styled(CardBase)`
  margin-top: 32px;
`;

const Empty = styled.div`
  width: 25px;
  height: 25px;
  display: none;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 56px 16px 56px 64px;
  
  ${SimpleProgression} {
    display: none;
  }
  
  @media(max-width: 980px) {
    padding: 40px 16px;
    align-items: center;
    
    ${Title} {
      margin-top: 28px;
      font-size: 16px;
      max-width: 220px;
      
      svg {
        font-size: 40px;
        width: 40px;
      }
    }
    
    ${Card} {
      margin-top: 16px;
    }
    
    ${Link} {
      font-size: 0;
      
      svg {
        font-size: 20px;
      }
    }
    
    ${SimpleProgression}, ${Empty} {
      display: block;
    }
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`;

const CheckoutAside = () => {
  return (
    <Container>
      <Header>
        <Link href="/" iconPosition="left">Alterar forma de pagamento</Link>
        <SimpleProgression current={2} total={3} />
        <Empty />
      </Header>
      <Title iconName="card">Adicione um novo cartão de crédito</Title>
      <Card />
    </Container>
  );
};

export default CheckoutAside;
