import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Icon from './Icon';

const Container = styled.div`
  border: 1px solid ${props => props.theme.red};
  border-radius: 50%;
  width: 22px;
  height: 22px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Number = styled.div`
  font-family: 'SF Pro';
  font-size: 14px;
  color: ${props => props.theme.red};
  font-weight: 600;
`;

const Completed = styled(Icon).attrs({
  name: 'check-circle',
})`
  font-size: 22px;
`;

const StepBullet = ({ step, completed, ...props }) => {
  if (completed) return <Completed />;
  return (
    <Container {...props}>
      <Number>{step}</Number>
    </Container>
  );
};

StepBullet.propTypes = {
  step: PropTypes.number.isRequired,
  completed: PropTypes.bool.isRequired,
};

export default StepBullet;
