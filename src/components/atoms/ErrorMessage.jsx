import styled from 'styled-components';

export default styled.span`
  color: ${props => props.theme.error};
  font-size: 13px;
`;
