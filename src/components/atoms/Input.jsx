import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MaskedInput from "react-text-mask";
import DownArrow from '../../assets/icons/down-arrow.svg'

const StyledInput = styled.input`
  appearance: none;
  background-color: transparent;
  width: 100%;
  border-bottom: 1px solid ${props => (props.isInvalid ? props.theme.error : props.theme.lightGray)};
  font-size: 17px;
  color: ${props => props.theme.darkGray};
  transition: 0.4s border ease;
  height: 32px;
`;

const Masked = styled(StyledInput).attrs({
  as: MaskedInput
})``;

const StyledSelect = styled(StyledInput).attrs({
  as: 'select',
})`
  position: relative;
  background: url(${DownArrow}) right center no-repeat;
  background-size: 20px 16px;
  padding-right: 24px;
  
  + label {
    pointer-events: none;
  }
`;

const Input = ({ type, mask, ...props }) => {
  if (mask) return <Masked type={type} mask={mask} {...props} />;
  else if (type === 'select') return <StyledSelect {...props} />;
  return <StyledInput {...props} />;
};

Input.propTypes = {
  type: PropTypes.string.isRequired,
  mask: PropTypes.array,
};

Input.defaultProps = {
  mask: null,
};

export default Input;
