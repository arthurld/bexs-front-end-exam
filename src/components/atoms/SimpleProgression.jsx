import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Text = styled.p`
  font-size: 13px;
  color: ${props => props.theme.white};
`;

const Current = styled.strong`
  font-weight: 700;
`;

const SimpleProgression = ({ current, total, ...props }) => {
  return (
    <Text {...props}><Current>Etapa: {current}</Current> de {`${total}`}</Text>
  );
};

SimpleProgression.propTypes = {
  current: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
};

export default SimpleProgression;
