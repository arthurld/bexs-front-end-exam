import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';
import customIcons from '../../helpers/custom-icons'

const FontAwesome = styled(FontAwesomeIcon)`
  font-size: 13px;
`;

// For icons to work you need add them to the library first. See in helpers folder
const Icon = ({ name, ...props }) => {
  let Component = FontAwesome;
  if (customIcons[name]) Component = customIcons[name];
  return (
    <Component icon={name} {...props} />
  );
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Icon;
