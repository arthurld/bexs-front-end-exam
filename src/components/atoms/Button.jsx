import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes, css } from "styled-components";

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
  transform: rotate(360deg);
  }
`;

const StyledButton = styled.button`
  background: ${props => props.theme.red};
  border: 3px solid ${props => props.theme.red};
  border-left-color: transparent;
  padding: 16px 72px;
  color: ${props => props.theme.white};
  font-size: 17px;
  text-transform: uppercase;
  font-family: 'SF Pro';
  letter-spacing: 1px;
  border-radius: 10px;
  cursor: pointer;
  transition: 0.4s width ease, 0.4s padding ease, 0.4s height ease;
  
  @media(max-width: 335px) {
    font-size: 14px;
  }
  
  ${props => props.isLoading && css`
    font-size: 0;
    background: transparent;
    border-radius: 50%;
    padding: 0;
    height: 53px;
    width: 53px;
    pointer-events: none;
    animation: 1s ${spin} linear 0.2s infinite;
  `}
`;

const Button = ({ href, loading, ...props }) => {
  return href ? <StyledButton as="a" href={href} /> : <StyledButton isLoading={loading} {...props} />
};

Button.propTypes = {
  href: PropTypes.string,
  loading: PropTypes.bool,
};

Button.defaultProps = {
  href: null,
  loading: false,
};

export default Button;
