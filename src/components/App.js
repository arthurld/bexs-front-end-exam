import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import sanitize from 'styled-sanitize';
import { Provider } from "mobx-react";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'
import * as stores from '../stores';
import fonts from '../helpers/fonts';
import colors from '../helpers/colors';
import '../helpers/icon-library';

import Checkout from './pages/Checkout'

const GlobalStyles = createGlobalStyle`
  ${sanitize};
  ${fonts};
  h1, h2, h3, h4, h5, h6 {
    margin:0;
    padding: 0;
  }
  html {
    height: 100%;
  }
  body {
    display: flex;
    flex-direction: column;
    min-height: 100%;
    height: 100%;
    font-family: Verdana, sans-serif;
    font-weight: 400;
  }
  
  #root {
    height: 100%;
  }
  
  a {
    text-decoration: none;
  }
  
  input, select, textarea, button {
    border: 0;
  }
`;

const App = () => (
  <>
    <GlobalStyles />
    <Provider {...stores}>
      <ThemeProvider theme={colors}>
        <>
          <Checkout />
          <ToastContainer />
        </>
      </ThemeProvider>
    </Provider>
  </>
);

export default App;
