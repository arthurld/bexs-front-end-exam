import { observable } from "mobx";

class Card {
  number = observable.box('');
  expireDate = observable.box('00/00');
  name = observable.box('NOME DO TITULAR');
  cvv = observable.box('');
  flipped = observable.box(false);

  setNumber(number) {
    const _number = number.replace(/\D/g, '');
    this.number.set(_number);
  }

  setExpireDate(date) {
    if (!date) return this.expireDate.set('00/00');
    this.expireDate.set(date);
  }

  setName(name) {
    if (!name) return this.name.set('NOME DO TITULAR');
    this.name.set(name);
  }

  setCvv(number) {
    const _number = number.replace(/\D/g, '');
    this.cvv.set(_number);
  }

  setFlipped(value) {
    this.flipped.set(value);
  }
}

export const card = new Card();
