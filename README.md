# Bexs - Exame Frontend

## Instalação
Na raiz do projeto executar:
### `npm install`
ou
### `yarn`

## Scripts

### `npm start`

Roda o projeto em modo de desenvolvimento<br>
O browser será aberto automaticamente em [http://localhost:3000/](http://localhost:3000/).

### `npm run build`

Builda o projeto para produção, otimizando a build tendo assim melhor performance, localizado na pasta `build` na raiz do projeto.<br>

### `npm run eject`

Caso precise fazer alguma modificação no webpack será necessário rodar este comando.
